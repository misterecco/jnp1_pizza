#include <cstdio>
#include <array>

template <typename... Kinds>
struct Pizzeria;

template <typename Pizzeria, size_t... Quantities>
struct Pizza {

    private:

        template<size_t x>
        static constexpr size_t getValueFromTemplate() {
            return x;
        }

        template<size_t Quantity>
        static constexpr size_t slice() {
            return 2 * Quantity;
        }

    public:

        typedef Pizza<Pizzeria, slice<Quantities>()...> sliced_type;

        template<typename Kind>
        static constexpr size_t count() {
            return Pizzeria::template count<Kind, Quantities...>();
        }
        static constexpr std::array<size_t, Pizzeria::type_count()> as_array() {
            return {{getValueFromTemplate<Quantities>()...}};
        }

};

template <typename... Kinds>
struct Pizzeria {

    private:

        template<typename Kind, typename PizzeriaKind>
        static constexpr size_t compare_kinds() {
            return (std::is_same<Kind, PizzeriaKind>::value * 8);
        }

        //Getting position of Kind in Kinds...
        template<typename Kind>
        static constexpr size_t getPosition() {
            return 0; //should not happen
        }

        template<typename Kind, typename Head, typename... Tail>
        static constexpr size_t getPosition() {
            return std::is_same<Kind, Head>::value ? 0 : (getPosition<Kind, Tail...>() + 1);
        }

        //Getting quantities in Quantities... at position pos
        template<typename Kind, size_t pos, size_t cur_pos>
        static constexpr size_t count_helper() {
            return 0; //should not happen
        }

        template<typename Kind, size_t pos, size_t cur_pos, size_t Head, size_t... Tail>
        static constexpr size_t count_helper() {
            return cur_pos == pos ? Head : count_helper<Kind, pos, cur_pos + 1, Tail...>();
        }

        template<typename Kind>
        static constexpr size_t yummy_helper(size_t opt_n, int n) {
            return n < 0 ? opt_n :
                  yummy_helper<Kind>((Kind::yumminess(opt_n) < Kind::yumminess(n)
                        ? n : opt_n), n - 1);
        }

        template<typename Kind, size_t Quantities1, size_t Quantities2>
        static constexpr size_t join() {
            static_assert(Kind::yumminess(0) == 0,
                "Yumminess of 0 slices of this pizza is not 0");
            return yummy_helper<Kind>(0, Quantities1 + Quantities2);
        }

        template<typename Kind>
        static constexpr bool is_kind_in_pizzeria() {
            return false;
        }

        template<typename Kind, typename Head, typename... Tail>
        static constexpr bool is_kind_in_pizzeria() {
            return std::is_same<Kind, Head>::value || is_kind_in_pizzeria<Kind, Tail...>();
        }

        template<typename Kind>
        static constexpr bool is_kind_unique() {
            return true;
        }

        template<typename Kind, typename Head, typename... Tail>
        static constexpr bool is_kind_unique() {
            return !std::is_same<Kind, Head>::value && is_kind_unique<Kind, Tail...>()
                  && is_kind_unique<Head, Tail...>();
        }

    public:

        static_assert(is_kind_unique<Kinds...>(),
                  "Trying to make pizzeria with two indistinguishable pizza types");

        template<typename Kind>
        struct make_pizza {
            static_assert(is_kind_in_pizzeria<Kind, Kinds...>(),
                  "This kind of pizza is unavailable in this pizzeria");
            typedef Pizza<Pizzeria, compare_kinds<Kind, Kinds>()...> type;
        };

        template<typename Kind, size_t... Quantities>
        static constexpr size_t count() {
            return count_helper<Kind, getPosition<Kind, Kinds...>(), 0, Quantities...>();
        }

        static constexpr size_t type_count() {
            return sizeof...(Kinds);
        }

        template<typename Pizza1, typename Pizza2>
        struct PizzaMixer;

        template <typename Pizzeria1, typename Pizzeria2, size_t... Quantities1, size_t... Quantities2>
        struct PizzaMixer<Pizza<Pizzeria1, Quantities1...>, Pizza<Pizzeria2, Quantities2...>> {
            typedef Pizza<Pizzeria, join<Kinds, Quantities1, Quantities2>()...> type;
        };
};

template <typename Pizza1, typename Pizza2>
struct best_mix;

template <typename Pizzeria1, typename Pizzeria2, size_t... Quantities1, size_t... Quantities2>
struct best_mix<Pizza<Pizzeria1, Quantities1...>, Pizza<Pizzeria2, Quantities2...>> {

     static_assert(std::is_same<Pizzeria1, Pizzeria2>::value,
             "These pizzas are form two different pizzerias");
     typedef typename Pizzeria1::template
              PizzaMixer<Pizza<Pizzeria1, Quantities1...>,
                         Pizza<Pizzeria1, Quantities2...>>::type type;
};
